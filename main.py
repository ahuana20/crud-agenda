from flask import Flask
from environment import Environment


def createAPP():
    app = Flask(__name__)
    from routes import bP
    app.register_blueprint(bP)
    return app


if __name__ == '__main__':
    config = Environment().general()
    app = createAPP()
    app.run(port=config['PORT'], debug=config['DEBUG'])
